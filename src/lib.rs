//use bytes::Bytes;
use std::io::Read;
//use pyo3::types::IntoPyDict;
//use pyo3::prelude::*;
use std::collections::HashMap;
use regex::Regex;
use std::fs;
//use pyo3::types::PyDict;
//use pyo3_file::PyFileLikeObject;
use std::sync::mpsc::TryRecvError;
//use std::io::Write;
//use std::io::Read;
//use pyo3::types::PyTuple;
//use pyo3::prelude::*;
//use std::path::Path;
//use structopt::StructOpt;
//use std::thread;
//use std::time::Duration;
//use rand::seq::SliceRandom;
//use std::sync::mpsc::channel;


pub fn parse_cookie_file<'a>(cookiefile: &str) -> (&'a str, HashMap<String, String>) {
    //((&'a str, &'a str), (&'a str, HashMap<String, String>)) {
    //let mut cookiedict = PyDict::new(py);
    let mut cookies: HashMap<String, String> = HashMap::new();
    let mut cookiefiledata = fs::read_to_string(cookiefile).unwrap();
    cookiefiledata = cookiefiledata.replace("\r", "");
    let re = Regex::new(r"^\#").unwrap();
    for line in cookiefiledata.lines() {
        //println!("'{}'", line);
        if !re.is_match(line) && line != "" {
            let linefields = line.trim().split("\t").collect::<Vec<&str>>();
            //println!("'{}':'{}'", linefields[5], linefields[6]);
            cookies.insert(linefields[5].to_string(), linefields[6].to_string());
        }
    }
    //let mut finaldict: HashMap<String, String>, String>> = HashMap::new();
    //finaldict.insert("value".to_string(), "http-cookies".to_string());
    //(("key", "http-cookies"), ("value", cookies))
    ("http-cookies", cookies)
}
pub fn keepgoing<'a>(rx: &std::sync::mpsc::Receiver<&'a str>) -> &'a str {
    let status = match rx.try_recv() {
        Ok(rx) => rx,
        Err(TryRecvError::Empty) => "empty",
        Err(TryRecvError::Disconnected) => "disconnected",
    };
    status
}
pub fn checkfo(fo: impl Read) -> (&'static str, Vec<u8>) {
    let mut databuf: Vec<u8> = vec![]; //[0u8; 1024];
    let mut chunk = fo.take(1024);
    //let rresult = match fo.read_exact(&mut databuf) {
    //    Ok(_x) => "good",
    //    Err(err) if err.kind() == std::io::ErrorKind::Interrupted => "Interrupted",
    //    Err(err) if err.kind() == std::io::ErrorKind::UnexpectedEof => "UnexpectedEOF",
    //    Err(_) => "Error"
    //};
    let rresult = match chunk.read_to_end(&mut databuf) {
        Ok(_x) => "good",
        Err(_) => "UnexpectedEOF",
    };
    //let rresult = "good";
    (rresult, databuf)
}
