//use std::collections::HashMap;
//use std::io::Read;
use std::io::Write;
//use std::io::Read;
//use pyo3::types::PyTuple;
//use pyo3::prelude::*;
use rand::seq::SliceRandom;
use std::fs;
use std::path::Path;
use std::thread;
use std::time::Duration;
use structopt::StructOpt;
//use std::process::{Command, Stdio};
//use pyo3_file::PyFileLikeObject;
use std::sync::mpsc::channel;
//use std::io::IoSliceMut;
//use iowrap::Eof;
/// Records youtube livestreams
#[derive(StructOpt, Debug)]
#[structopt(name = "recordls")]
struct Opt {
    /// YouTube URL
    #[structopt(name = "YOUTUBE_URL")]
    url: String,
    /// Output file path
    #[structopt(name = "FILE_PATH")]
    filename: String,
}
//#[tokio::main]
fn main() {
    let (tx, rx) = channel();
    //let mut tx1 = tx.clone();

    ctrlc::set_handler(move || tx.send("ctrlc").expect("Could not send signal on channel."))
        .expect("Error setting Ctrl-C handler");
    let opt = Opt::from_args();
    let mut filenametxt = fs::File::create("filename.txt").unwrap(); //.trim().to_string();
    filenametxt.write_all(opt.filename.as_bytes()).unwrap();
    /*Python::with_gil(|py| {
    let cookiedict = recordlspy::parse_cookie_file("cookies.txt");
    let cdict = cookiedict.clone();
    let streamlink: &PyModule = PyModule::import(py, "streamlink").unwrap();
    let ytunshorten: &PyModule = PyModule::from_code(py, fs::read_to_string("ytunshorten.py").unwrap().as_str(), "ytunshorten.py", "ytunshorten").unwrap();
    let finalurl: String = ytunshorten.call_method1("unshorten", (&opt.url,)).unwrap().extract().unwrap();
    let mut session: &PyAny = streamlink.call_method0("Streamlink").unwrap();
    //session.call_method1("set_option", cookiedict).unwrap();
    //let argtuple = opt.url.to_object(py);
    //let code = format!("('{}',)", opt.url);
    //let args: &Py*/
    let sleepchoices = vec![
        Duration::new(5, 0),
        Duration::new(6, 0),
        Duration::new(7, 0),
        Duration::new(8, 0),
        Duration::new(9, 0),
    ];
    //let args = py.eval(code.as_str(), None, None).unwrap().downcast::<PyTuple>().unwrap();
    //let args = (&finalurl,);
    //let mut streams: &PyAny = session.call_method1("streams", args).unwrap();*/
    //let mut keeplooping = true;
    //let mut firsttime = true;
    //let mut skipsleep = true;
    //let mut badresult = false;
    let mut session = ytlivestream::session::Session::new(opt.url.as_str().to_string());
    let mut streams = session.sessions();
    //while keeplooping && recordlspy::keepgoing(&rx) != "ctrlc"{
    /*if !skipsleep {
        //println!("Top sleep");
        thread::sleep(*sleepchoices.choose(&mut rand::thread_rng()).unwrap());
        skipsleep = true;
    }
    if !firsttime {
        //println!("Not first time");
        /*session = streamlink.call_method0("Streamlink").unwrap();
        session.call_method1("set_option", cdict.clone()).unwrap();
        streams = session.call_method1("streams", args).unwrap();*/
        streams = session.sessions()
    }
    */
    while streams.is_err() && recordls::keepgoing(&rx) != "ctrlc" {
        //println!("streams is none");
        match streams {
            Ok(ref _x) => continue,
            Err(ytlivestream::session::StreamError::NotLive(x)) => println!("{}", x),
            Err(ytlivestream::session::StreamError::NotLiveContent(x)) => println!("{}", x),
            Err(ytlivestream::session::StreamError::ParsingError(x)) => println!("{}", x),
        }
        thread::sleep(*sleepchoices.choose(&mut rand::thread_rng()).unwrap());
        //skipsleep = true;
        session = ytlivestream::session::Session::new(opt.url.as_str().to_string());

        streams = session.sessions();
        //streams = session.call_method1("streams", args).unwrap();
    }

    /*keeplooping = match streams["best"] {
        Ok(_x) => false,
        Err(_) => true,
    };
    if keeplooping {
        skipsleep = false;
        badresult = true;
    } else {
        badresult = false;
    }
    firsttime = false;*/
    //}
    //if badresult {
    //    return;
    //}
    //let mut gstream: Box<HashMap<String, Box<reqwest::blocking::RequestBuilder>>> = streams.unwrap();//["best"].send().unwrap();
    //let mut goodstreamboxed: &reqwest::blocking::RequestBuilder = gstream["best"].as_mut();
    //println!("'{}'", &streams.unwrap()["best"].as_str());
    //return;
    //let mut gstream = session.http.get(streams.unwrap()["best"].as_str()).send().unwrap();
    //let goodstream = gstream.by_ref();
    //.send().unwrap();
    /*let gsf = goodstream.call_method0("open").unwrap();
    let gsfobj = PyObject::from(gsf);
    let mut flobj = PyFileLikeObject::new(gsfobj);
    */
    //let mut newobj = Eof::new(flobj);
    //let mut data = IoSliceMut::new(&mut databuf);
    //let mut bufdata = [data];
    //flobj.read(&mut bufdata).unwrap();
    //let mut biter = flobj.bytes();

    //for outdata in bufdata[0].chunks(4096) {
    //    if recordlspy::keepgoing(&rx) != "ctrlc" {
    //        outfile.write_all(outdata).unwrap();
    //    } else {
    //        println!("Got ctrl-c");
    //        break;
    //    }
    //}
    //let mut byte = biter.next();
    session = ytlivestream::session::Session::new(opt.url.as_str().to_string());
    streams = session.sessions();
    let goodurl = streams.unwrap();
    //let argstring = format!("-i {} -codec copy -timeout 5 {}", goodurl, opt.filename);
    //let mut shfile = fs::File::create("runffmpeg.sh").unwrap();
    //shfile.write_all(argstring.as_bytes()).unwrap();
    //let mut ffmpegchild = Command::new("ffmpeg").arg("-i").arg("-").arg("-f").arg("hls").arg("-hls_list_size").arg("10").arg("-hls_time").arg("2").arg("out.m3u8")
    //let mut child = Command::new("ffmpeg").args(argstring.split(" "))
    //    .stdin(Stdio::null())
    //    .stdout(Stdio::piped())
    //    .stderr(Stdio::null())
    //    .spawn().unwrap();
    //let mut ffmpegout = Box::new(child.stdout.take().unwrap());
    //println!("{}", goodurl);
    //return;
    //let mut child2 = &child;
    //thread::spawn(move || {
    //    child2.wait();
    //    tx1.send("ctrlc");
    //});
    //let mut rresult = "good";
    gotify::send_notification("recordls", "Started recording", 10);
    if !Path::new("RECORDINGNOW.tmp").exists() {
        fs::File::create("RECORDINGNOW.tmp").unwrap();
    }
    let mut outfile = fs::File::create(opt.filename).unwrap();
    while recordls::keepgoing(&rx) != "ctrlc" {
        //&& rresult == "good" {
        //thread::sleep(Duration::new(1, 0));
        //let mut databuf: Vec<u8> = vec![];
        //let mut chunk = ffmpegout.as_mut().take(1000000);
        //rresult = match chunk.read_to_end(&mut databuf) {
        //    Ok(_x) => "good",
        //    Err(err) if err.kind() == std::io::ErrorKind::Interrupted => "Interrupted",
        //    Err(err) if err.kind() == std::io::ErrorKind::UnexpectedEof => "UnexpectedEOF",
        //    Err(_) => "Error",
        //};
        //rresult = match chunk.read_to_end(&mut databuf) {
        //    Ok(_x) => "good",
        //    Err(_) => "UnexpectedEOF",
        //};
        //let blah = recordlspy::checkfo(goodstream);
        //let rresult = blah.0;
        //let databuf = blah.1;
        //println!("{:?}", databuf);
        //println!("{:?}", &databuf);
        //let mut rresult = String::new();
        match session.read(goodurl.as_str().to_string()) {
            Ok(x) => {
                outfile.write_all(&x).unwrap();
            }
            Err(err) if err.kind() == std::io::ErrorKind::Interrupted => eprintln!("{}", err),
            Err(err) if err.kind() == std::io::ErrorKind::UnexpectedEof => {
                eprintln!("{}", err);
                break;
            }
            Err(err) => {
                eprintln!("{}", err);
                break;
            }
        }
        //outfile.write_all(&session.read(goodurl.as_str().to_string()).unwrap()).unwrap();
        //if rresult == "UnexpectedEOF" {
        //    break;
        //}
        //byte = biter.next();
    }
    //nix::sys::signal::kill(
    //    nix::unistd::Pid::from_raw(child.id() as i32),
    //    nix::sys::signal::Signal::SIGINT
    //).expect("cannot send ctrl-c");
    //child.wait().unwrap();

    //gsf.getattr("close").unwrap().call0().unwrap();
    gotify::send_notification("recordls", "Done recording", 10);
    //let breader = BufReader::new(flobj);

    //});
    if Path::new("RECORDINGNOW.tmp").exists() {
        fs::remove_file("RECORDINGNOW.tmp").unwrap();
    }
    //println!("Hello, world!");
}
